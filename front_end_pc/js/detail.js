var vm = new Vue({
    el: '#app',
    delimiters: ['[[', ']]'],
    data: {
    },
    mounted: function(){
        // 调用
        this.detail_visit();
    },
    methods: {
        // 新增记录商品详情的访问量
        detail_visit(){
            if (this.category_id) {
                var url = this.hots + '/detail/visit/' + this.category_id + '/';
                axios.post(url, {}, {
                    responseType: 'json',
                    withCredentials:true,
                })
                    .then(response => {
                        console.log(response.data);
                    })
                    .catch(error => {
                        console.log(error.response);
                    });
            }
        },
});
