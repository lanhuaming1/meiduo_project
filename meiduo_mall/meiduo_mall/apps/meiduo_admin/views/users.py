from rest_framework import serializers
from rest_framework.generics import ListAPIView, ListCreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from meiduo_admin.models import UserModerSerializer
from meiduo_admin.utils import PageNum
from users.models import User

# class SearchViewoo(APIView):
#
#
#     def get(self, request):
#         pagination_class = PageNum
#         keyword = request.GET.get('keyword')
#         page = request.GET.get('page')
#         pagesize = request.GET.get('pagesize')
#
#         if keyword:
#
#             user = User.objects.filter(username__contains=keyword)
#         else:
#             user = User.objects.all()
#
#         lists = []
#         for person in user:
#             lists.append({
#                 'id': person.id,
#                 'username': person.username,
#                 'mobile': person.mobile,
#                 'email': person.email
#             })
#         print(int(user.count() / int(pagesize) + 1))
#
#         return Response({
#             'count': user.count(),
#             'lists': lists,
#             'page': page,
#             'pagesize': int(pagesize),
#             'pages': int(user.count() / int(pagesize) + 1)
#         })


'''
    page_size = 5
    page_size_query_param = 'pagesize'
    max_page_size = 10
'''


class SearchViewoo(ListAPIView):
    serializer_class = UserModerSerializer
    pagination_class = PageNum

    def get_queryset(self):
        keyword = self.request.query_params.get('keyword')

        if keyword:
            return User.objects.filter(username__contains=keyword)
        else:
            return User.objects.all()


class AddUserView(ListCreateAPIView):
    serializer_class = UserModerSerializer
    pagination_class = PageNum

    def get_queryset(self):
        keyword = self.request.query_params.get('keyword')
        if keyword:
            return User.objects.filter(username__contains=keyword)
        else:
            return User.objects.all()




