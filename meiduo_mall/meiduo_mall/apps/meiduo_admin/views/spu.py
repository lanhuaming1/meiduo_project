from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from goods.models import Goods, Brand, GoodsCategory
from meiduo_admin.serializers.spu import SPUAllSerializer, BrandListSerializer, GoodsCategoryFirstSerializer, \
    TestSerializer
from meiduo_admin.utils import PageNum


class SPUAllView(ModelViewSet):
    '创建所有商品显示视图'
    serializer_class = SPUAllSerializer
    pagination_class = PageNum

    queryset = Goods.objects.all()

class SPUAllViewppp(APIView):


    def post(self, request):

        rcv_data = request.data
        request.data['id'] = 1
        request.data['category1'] = request.data.get('category1_id')
        request.data['category2'] = request.data.get('category2_id')
        request.data['category3'] = request.data.get('category3_id')
        request.data['brand'] = request.data.get('brand_id')
        print(rcv_data)
        serializer = SPUAllSerializer(data=rcv_data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response({'msg': 'error'})


class BrandListView(ModelViewSet):
    serializer_class = BrandListSerializer
    queryset = Brand.objects.all()


class GoodsCategoryFirstView(ModelViewSet):
    serializer_class = GoodsCategoryFirstSerializer
    queryset = GoodsCategory.objects.filter(parent_id=None)
    # queryset = GoodsCategory.objects.filter(parent_id=None)


class GoodsCategoryLowerView(APIView):
    '''二三级预先框'''

    def get(self, request, pk):
        # queryset = GoodsCategory.objects.all()
        queryset = GoodsCategory.objects.filter(parent_id=pk)

        serializer = TestSerializer(queryset, many=True)

        return Response(serializer.data)
