from rest_framework.generics import ListAPIView
from rest_framework.viewsets import ModelViewSet

from goods.models import SKU, GoodsCategory, Goods, GoodsSpecification

from meiduo_admin.serializers.sku import SKUSerializer, GoodsCategorySerializer, SPUSerializer, GoodsSpecSerializer, \
    SKUKeepSerializer
from meiduo_admin.utils import PageNum


class SkuGetView(ModelViewSet):
    serializer_class = SKUKeepSerializer
    pagination_class = PageNum

    def get_queryset(self):
        keyword = self.request.query_params.get('keyword')

        if keyword:
            return SKU.objects.filter(name__contains=keyword)

        else:
            return SKU.objects.all()




class GoodsCategoryView(ModelViewSet):
    serializer_class = GoodsCategorySerializer
    queryset = GoodsCategory.objects.all()


class SPUView(ModelViewSet):
    serializer_class = SPUSerializer
    queryset = Goods.objects.all()


class GoodsDetailView(ListAPIView):
    serializer_class = GoodsSpecSerializer

    def get_queryset(self):
        pk = self.kwargs['pk']
        return GoodsSpecification.objects.filter(spu_id=pk)


# class SKUKeepView(ModelViewSet):
#     serializer_class = SKUKeepSerializer
#     queryset = SKU.objects.all()
