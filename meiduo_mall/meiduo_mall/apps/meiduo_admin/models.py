from django.db import models
from rest_framework import serializers
from rest_framework.serializers import Serializer

from users.models import User


class UserModerSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

        extra_kwargs = {
            'username': {
                'max_length': 20,
                'min_length': 5,
            },
            'password': {
                'max_length': 20,
                'min_length': 8,
                'write_only': True
            }
        }

    def create(self, validated_data):
        validated_data.setdefault('is_staff', True)
        validated_data.setdefault('is_superuser', True)
        user = User.objects.create_user(**validated_data)

        return user
