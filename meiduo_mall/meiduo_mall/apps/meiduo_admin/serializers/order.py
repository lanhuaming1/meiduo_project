from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from goods.models import SKU
from orders.models import OrderInfo, OrderGoods


class SKUSerializer(ModelSerializer):
    # spu_id = serializers.IntegerField()
    # category_id = serializers.IntegerField()

    class Meta:
        model = SKU
        fields = '__all__'
        fields = ('name', 'default_image_url')


class OrderGoodsSerializer(ModelSerializer):
    # order_id = serializers.IntegerField()
    # sku_id = serializers.IntegerField()
    # order = serializers.StringRelatedField(read_only=True)
    sku = SKUSerializer()

    class Meta:
        model = OrderGoods
        # fields = '__all__'
        fields = ('count', 'price', 'sku')


class OrderInfoSerializer(ModelSerializer):
    order_id = serializers.IntegerField()
    user = serializers.StringRelatedField(read_only=True)
    skus = OrderGoodsSerializer(many=True)

    class Meta:
        model = OrderInfo
        fields = ('order_id', 'user', 'total_count', 'total_amount', 'freight',
                  'pay_method', 'status', 'create_time', 'skus')


class OrderInfolitterSerializer(ModelSerializer):
    class Meta:
        model = OrderInfo
        fields = '__all__'


class OrderStatusSerializer(ModelSerializer):
    class Meta:
        model = OrderInfo
        fields = '__all__'
