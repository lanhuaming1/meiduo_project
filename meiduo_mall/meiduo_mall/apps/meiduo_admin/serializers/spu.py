from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from goods.models import Goods, Brand, GoodsCategory


class SPUAllSerializer(ModelSerializer):
    # id = serializers.IntegerField()
    category1_id = serializers.IntegerField()
    category2_id = serializers.IntegerField()
    category3_id = serializers.IntegerField()
    category1 = serializers.StringRelatedField()
    category2 = serializers.StringRelatedField()
    category3 = serializers.StringRelatedField()

    brand_id = serializers.IntegerField()
    sales = 0
    comments = ''

    class Meta:
        model = Goods
        # fields = ("id",
        #           "category1_id",
        #           "category2_id",
        #           "category3_id",
        #           "brand_id",
        #           "create_time",
        #           "update_time",
        #           "name",
        #           "sales",
        #           "comments",
        #           "desc_detail",
        #           "desc_pack",
        #           "desc_service",
        #           "brand",
        #           "category1",
        #           "category2",
        #           "category3")
        fields = '__all__'


class BrandListSerializer(ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = Brand
        fields = ('id', 'name')


class GoodsCategoryFirstSerializer(ModelSerializer):
    ''' 建立一级分类信息'''
    id = serializers.IntegerField()

    class Meta:
        model = GoodsCategory
        fields = ('id', 'name')


class TestSerializer(ModelSerializer):
    ''' 建立一级分类信息'''

    class Meta:
        model = GoodsCategory
        fields = '__all__'
