import rest_framework.serializers as serializers

from goods.models import SKU, SKUImage


class SkuImageView(serializers.ModelSerializer):
    sku = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = SKUImage
        fields = ('sku', 'image', 'id')


class SKUSerialize(serializers.ModelSerializer):
    class Meta:
        model = SKU
        fields = ('id', 'name')
