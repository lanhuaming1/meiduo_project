from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView

from meiduo_admin.models import UserModerSerializer
from users.models import User


class AuthorView(APIView):
    def post(self, request):

        user_dict = request.data
        serialize = UserModerSerializer(data=user_dict)

        if serialize.is_valid():
            serialize.save()
            return Response(serialize.data)
        else:
            return Response({'msg': '保存失败'})


