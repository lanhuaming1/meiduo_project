from django.urls import re_path
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token

from meiduo_admin import utils
# from meiduo_admin.serializers.image import ImagesShowView
from meiduo_admin.views import sku
from meiduo_admin.views.image import ImagesShowView, AddSimpleView
from meiduo_admin.views.order import OrderInfoView, OrderDetailView, OrderStatusView
from meiduo_admin.views.sku import SkuGetView, GoodsCategoryView, SPUView
from meiduo_admin.views.spu import SPUAllView, BrandListView, GoodsCategoryFirstView, GoodsCategoryLowerView, \
    SPUAllViewppp
from meiduo_admin.views.statistical import StatisticalView, DayilyActiveView, DayOrdersView, MonthIncrementView, \
    DayIncrementView, VisitView, GoodsDayView
from meiduo_admin.views.users import SearchViewoo, AddUserView

from . import views

urlpatterns = [
    re_path(r'^authorizations/$', obtain_jwt_token),
    re_path(r'^statistical/total_count/$', StatisticalView.as_view()),
    re_path(r'^statistical/day_active/$', DayilyActiveView.as_view()),
    re_path(r'^statistical/day_orders/$', DayOrdersView.as_view()),
    re_path(r'^statistical/day_increment/$', DayIncrementView.as_view()),
    re_path(r'^statistical/month_increment/$', MonthIncrementView.as_view()),
    # re_path(r'^skus/images/$', ImagesShowView.as_view()),
    re_path(r'^users/$', AddUserView.as_view()),
    re_path(r'^skus/simple/$', AddSimpleView.as_view()),
    re_path(r'^goods/(?P<pk>\d+)/specs/$', sku.GoodsDetailView.as_view()),
    re_path(r'^statistical/goods_day_views/$', GoodsDayView.as_view()),
    re_path(r'^orders/(?P<pk>\d+)/$', OrderDetailView.as_view()),
    re_path(r'^orders/(?P<order_id>\w+)/status/$', OrderStatusView.as_view()),
    re_path(r'^goods/channel/categories/(?P<pk>\d+)/$', GoodsCategoryLowerView.as_view()),
    # re_path(r'^goods/$', SPUAllViewppp.as_view()),


]

router = DefaultRouter()

router.register(r'skus/images', ImagesShowView, basename='image')
urlpatterns += router.urls

router.register(r'skus/categories', GoodsCategoryView, basename='image')
urlpatterns += router.urls

router.register(r'skus', SkuGetView, basename='image')
urlpatterns += router.urls

router.register(r'goods', SPUAllView, basename='image')
urlpatterns += router.urls


router.register(r'goods/simple', SPUView, basename='image')

urlpatterns += router.urls

router.register(r'goods/brands/simple', BrandListView, basename='image')
urlpatterns += router.urls

router.register(r'goods/channel/categories', GoodsCategoryFirstView, basename='image')
urlpatterns += router.urls

router.register(r'orders', OrderInfoView, basename='image')
urlpatterns += router.urls




# urlpatterns += "re_path(r'^goods/channel/categories/(?P<pk>\d+)/$', GoodsCategoryLowerView.as_view())"

# urlpatterns.append(re_path(r'^goods/$', SPUAllViewppp.as_view()))
