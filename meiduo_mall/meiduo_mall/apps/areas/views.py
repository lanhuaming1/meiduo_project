from django.core.cache import cache
# from django_redis import cache

from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
from django.views import View


from areas.models import Area


class ProvinceAreasView(View):
    def get(self, request):
        try:
            # province_list = Area.objects.filter(parent='NULL')
            province_list = Area.objects.filter(parent__isnull=True)
        except Exception as e:
            return JsonResponse({
                'code': 400,
                'errmsg': '无法连接数据库'
            })
        province_list_all = []
        for province_model in province_list:
            action = {
                'id': province_model.id,
                'name': province_model.name
            }
            province_list_all.append(action)
        return JsonResponse({
            'code': 0,
            'errmsg': 'ok',
            'province_list': province_list_all
        })


# class SubAreasView(View):
#     def get(self, request, pk):
#         try:
#             city_list = Area.objects.filter(parent=pk)
#         except Exception as e:
#             return JsonResponse({
#                 'code': 400,
#                 'errmsg': '查询数据库报错'
#             })
#         city_lists = []
#         for city_model in city_list:
#             city = {
#                 'id': city_model.id,
#                 'name': city_model.name
#             }
#             city_lists.append(city)
#         try:
#             province_model = Area.objects.get(id=pk)
#         except Exception as e:
#             return JsonResponse({
#                 'code': 400,
#                 'errmsg': '城市报错'
#             })
#         sub_data = {
#             'id': province_model.id,
#             'name': province_model.name,
#             'subs': city_lists
#         }
#         return JsonResponse({
#             'code': 0,
#             'errmsg': 'ok',
#             'sub_data': sub_data
#         })


class SubAreasView(View):
    def get(self, request, pk):
        sub_data = cache.get('sub_area_' + pk)

        if not sub_data:
            try:
                try:
                    city_list = Area.objects.filter(parent=pk)
                except Exception as e:
                    return JsonResponse({
                        'code': 400,
                        'errmsg': '查询数据库报错'
                    })
                city_lists = []
                for city_model in city_list:
                    city = {
                        'id': city_model.id,
                        'name': city_model.name
                    }
                    city_lists.append(city)
                try:
                    province_model = Area.objects.get(id=pk)
                except Exception as e:
                    return JsonResponse({
                        'code': 400,
                        'errmsg': '城市报错'
                    })
                sub_data = {
                    'id': province_model.id,
                    'name': province_model.name,
                    'subs': city_lists
                }
                cache.set('sub_area_' + pk, sub_data, 3600)
            except Exception as e:
                return JsonResponse({
                    'code': 400,
                    'errmsg': 's缓存数据失败'
                })
        return JsonResponse({
            'code': 0,
            'errmsg': 'ok',
            'sub_data': sub_data
        })
