# Generated by Django 2.2.5 on 2020-05-25 13:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goods', '0003_auto_20200525_1303'),
    ]

    operations = [
        migrations.RenameField(
            model_name='goodsspecification',
            old_name='goods',
            new_name='spu',
        ),
    ]
