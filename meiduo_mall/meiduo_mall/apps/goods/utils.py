
import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'meiduo_mall.settings.dev')
import django
django.setup()

from django.conf import settings
from django.http import JsonResponse
from django.template import loader
import os

from goods.models import SKU, SKUImage, SKUSpecification


def get_breadcrumb(category):
    breadcrumb = {
        'cat1': '',
        'cat2': '',
        'cat3': '',
    }

    if category.parent is None:

        breadcrumb['cat1'] = category.name

    elif category.parent.parent is None:
        breadcrumb['cat2'] = category.name
        breadcrumb['cat1'] = category.parent.name

    else:
        breadcrumb['cat3'] = category.name
        cat2 = category.parent
        breadcrumb['cat2'] = cat2.name
        breadcrumb['cat1'] = cat2.parent.name

    return breadcrumb


# def get_goods_and_spec(sku_id):
#     try:
#         sku = SKU.objects.get(id=sku_id)
#         sku.images = SKUImage.objects.filter(sku=sku)
#     except Exception as e:
#         return JsonResponse({
#             'code': 400,
#             'errmsg': '获取数据失败'
#         })
#
#     sku_specs = SKUSpecification.objects.filter(sku=sku).order_by('spec_id')
#
#     sku_key = []
#
#     for spec in sku_specs:
#         sku_key.append(spec.option.id)
#
#     goods = sku.goods
#
#     skus = SKU.objects.filter(goods=goods)
#
#     dict = {}
#
#     for temp_sku in skus:
#         s_specs = SKUSpecification.objects.filter(sku=temp_sku).order_by('spec_id')
#
#         key = []
#         for spec in s_specs:
#             key.append(spec.option.id)
#
#         dict[tuple(key)] = temp_sku.id
#
#     goods_specs = goods.goodsspecification_set.order_by('id')
#
#     for index, spec in enumerate(goods_specs):
#
#         key = sku_key[:]
#
#         spec_options = spec.specificationoption_set.all()
#
#         for options in spec_options:
#             key[index] = options.id
#             options.sku_id = dict.get(tuple(key))
#
#         spec.spec_options = spec_options
#
#     context = {
#         'categories': categories,
#         'goods': goods,
#         'specs': goods_specs,
#         'sku': sku
#     }
#
#
#     template = loader.get_template('detail.html')
#     html_text = template.render(context)
#     file_path = os.path.join(settings.GENERATED_STATIC_HTML_FILES_DIR,
#                              'goods/'+str(sku_id)+'.html')
#     with open(file_path, 'w', encoding='utf-8') as f:
#         f.write(html_text)
#
#
# if __name__ == '__main__':
#     # 获取所有的商品信息
#     skus = SKU.objects.all()
#     # 遍历拿出所有的商品:
#     for sku in skus:
#         print(sku.id)
#         # 调用我们之前在 celery_tasks.html.tasks 中写的生成商品静态页面的方法:
#         # 我们最好把这个函数单独复制过来, 这样可以不依靠 celery, 否则必须要开启celery
#         generate_static_sku_detail_html(sku.id)


# 导入:
from django import http
from collections import OrderedDict
from goods.models import GoodsCategory
from goods.models import GoodsChannel, SKU
from goods.models import SKUImage, SKUSpecification
from goods.models import GoodsSpecification, SpecificationOption


def get_goods_and_spec(sku_id):
    # ======== 获取该商品和该商品对应的规格选项id ========
    try:
        # 根据 sku_id 获取该商品(sku)
        sku = SKU.objects.get(id=sku_id)
        # 获取该商品的图片
        sku.images = SKUImage.objects.filter(sku=sku)
    except Exception as e:
        return http.JsonResponse({'code': 400,
                                  'errmsg': '获取数据失败'})

    # 获取该商品的所有规格: [颜色, 内存大小, ...]
    sku_specs = SKUSpecification.objects.filter(sku=sku).order_by('spec_id')

    sku_key = []
    # 获取该商品的所有规格后,遍历,拿取一个规格

    for spec in sku_specs:
        # 规格 ----> 规格选项 ----> 选项id  ---> 保存到[]
        sku_key.append(spec.option.id)

    # ======== 获取类别下所有商品对应的规格选项id ========
    # 根据sku对象,获取对应的类别
    goods = sku.goods

    # 获取该类别下面的所有商品
    skus = SKU.objects.filter(goods=goods)

    dict = {}
    for temp_sku in skus:
        # 获取每一个商品(temp_sku)的规格参数
        s_specs = SKUSpecification.objects.filter(sku=temp_sku).order_by('spec_id')

        key = []
        for spec in s_specs:
            # 规格 ---> 规格选项 ---> 规格选项id ----> 保存到[]
            key.append(spec.option.id)

        # 把 list 转为 () 拼接成 k : v 保存到dict中:
        dict[tuple(key)] = temp_sku.id

    # ======== 在每个选项上绑定对应的sku_id值 ========
    specs = GoodsSpecification.objects.filter(goods=goods).order_by('id')

    for index, spec in enumerate(specs):
        # 复制当前sku的规格键
        key = sku_key[:]
        # 该规格的选项
        spec_options = SpecificationOption.objects.filter(spec=spec)

        for option in spec_options:
            # 在规格参数sku字典中查找符合当前规格的sku
            key[index] = option.id
            option.sku_id = dict.get(tuple(key))

        spec.spec_options = spec_options

    return goods, specs, sku


def get_categories():
    # ======== 生成上面字典格式数据 ========
    # 第一部分: 从数据库中取数据:
    # 定义一个有序字典对象
    dict = OrderedDict()

    # 对 GoodsChannel 进行 group_id 和 sequence 排序, 获取排序后的结果:
    channels = GoodsChannel.objects.order_by('group_id', 'sequence')

    # 遍历排序后的结果: 得到所有的一级菜单( 即,频道 )
    for channel in channels:
        # 从频道中得到当前的 组id
        group_id = channel.group_id

        # 判断: 如果当前 组id 不在我们的有序字典中:
        if group_id not in dict:
            # 我们就把 组id 添加到 有序字典中
            # 并且作为 key值, value值是
            # {'channels': [], 'sub_cats': []}
            dict[group_id] = {
                'channels': [],
                'sub_cats': []
            }

        # 获取当前频道的分类名称
        cat1 = channel.category

        # 给刚刚创建的字典中, 追加具体信息:
        # 即, 给'channels' 后面的 [] 里面添加如下的信息:
        dict[group_id]['channels'].append({
            'id': cat1.id,
            'name': cat1.name,
            'url': channel.url
        })
        cat2s = GoodsCategory.objects.filter(parent=cat1)
        # 根据 cat1 的外键反向, 获取下一级(二级菜单)的所有分类数据, 并遍历:
        for cat2 in cat2s:
            # 创建一个新的列表:
            cat2.sub_cats = []
            # 获取所有的三级菜单
            cat3s = GoodsCategory.objects.filter(parent=cat2)
            # 遍历
            for cat3 in cat3s:
                # 把三级菜单保存到cat2对象的属性中.
                cat2.sub_cats.append(cat3)
            # 把cat2对象保存到对应的列表中
            dict[group_id]['sub_cats'].append(cat2)

    return dict
