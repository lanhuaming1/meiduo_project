from django.urls import re_path

from goods import views
from meiduo_admin.views.statistical import VisitView

urlpatterns = [
    re_path(r'^list/(?P<category_id>\d+)/skus/', views.ListView.as_view()),
    re_path(r'^hot/(?P<category_id>\d+)/$', views.HotGoodsView.as_view()),
    re_path(r'^search/$', views.MySearchView()),
    re_path(r'^detail/visit/(?P<category_id>\d+)/$', VisitView.as_view()),

]